import React from 'react';
import api from '../services/api';
import { useAuth } from './AuthContext';
import { UIContext } from './UIContext';

interface DataProps {
  data: any;
  search(value: string): Promise<any>;
  setData(e: any): void;
}

export const DataContext = React.createContext<DataProps | null>(null);

export const DataProvider: React.FC = ({ children }) => {
  const[data, setData] = React.useState({enterprises: []});
  const authContext = useAuth();
  const uiContext = React.useContext(UIContext);

  const search = React.useCallback( async ( value ) => {
    const response = await api.get(`enterprises?name=${value}`, {
      headers: {
        'access-token': authContext.token,
        'uid': authContext.uid,
        'client': authContext.client
      } 
    });
    uiContext?.setContent('erro');
    if(!!response.data.enterprises)
      setData(response.data.enterprises);

  },[])
  
  
  return (
    <DataContext.Provider value={{ data, setData, search }}>
      {children}
    </DataContext.Provider>
  )
}