import React, { createContext} from 'react';
import api from '../services/api';

interface SignInProps {
  email: string;
  password: string;
}

interface AuthContextProps {
  uid?: string,
  client?: string;
  token?: string;
  signIn(credentials: SignInProps):  Promise<void>;
}

interface AuthState {
  token?: string;
  uid?: string;
  client?: string;
}

const AuthContext = createContext<AuthContextProps | any>(null);

export const AuthProvider: React.FC = ({ children }) => {
  const [data, setData] = React.useState<AuthState>(() => {
    const token = localStorage.getItem('@ioasys:token');
    const uid = localStorage.getItem('@ioasys:uid');
    const client = localStorage.getItem('@ioasys:client');

    if ( token && uid && client) {
      return { token, uid, client }
    }

    return {};
  })

  const signIn = React.useCallback( async ({ email, password }) => {
    const response = await api.post('users/auth/sign_in', {
      email,
      password,
    });
    
    const headers = response.headers;
    const token = headers['access-token'];
    const uid = headers['uid'];
    const client = headers['client'];

    localStorage.setItem('@ioasys:token', token);
    localStorage.setItem('@ioasys:uid', uid);
    localStorage.setItem('@ioasys:client', client);

    setData({ token, uid, client});

  },[]);

  return (
    <AuthContext.Provider value={{token:data.token, client:data.client, uid:data.uid, signIn}}>
      {children}
    </AuthContext.Provider>
  )
}

export function useAuth(): AuthContextProps {
  const context = React.useContext(AuthContext);

  if(!context) {
    throw new Error('useAuth precisa ser usado com um AuthProvider');
  }

  return context;
}

export default AuthContext;
