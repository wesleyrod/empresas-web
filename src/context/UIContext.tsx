import React, { createContext} from 'react';

interface UIProps {
  content: string;
  setContent(value: string): void;
}

export const UIContext = React.createContext<UIProps | null>(null);

export const UIProvider: React.FC = ({ children }) => {
  const [content, setContent] = React.useState('initial');

  return (
    <UIContext.Provider value={{ content, setContent }}>
      {children}
    </UIContext.Provider>
  )
}



