import React from 'react';
import { UIContext } from '../../context/UIContext';
import { DataContext } from '../../context/DataContext';

import { Container, Logo, SearchIcon, SearchBar } from './styles';
import logotipo from '../../assets/logo-nav.png';
import { IoSearchSharp, IoCloseSharp } from 'react-icons/io5';

const Header: React.FC = () => {
  const [searchBar, setSearchBar] = React.useState(false);
  const inputRef = React.useRef<HTMLInputElement>(null);
  const uiContext = React.useContext(UIContext);
  const dataContext = React.useContext(DataContext);


  function handleSearch(value: string, searchBar: boolean) {
    setSearchBar(false);
    uiContext?.setContent(value);
  }

  function handleSubmit() {
    event?.preventDefault();
    alert(inputRef.current?.value);
    dataContext?.search(inputRef.current?.value || '');
  }

  return (
    <Container searchBarOpen={searchBar}>
      {searchBar
        ? <>
            <SearchBar>
              <SearchIcon>
                <IoSearchSharp size={40} color='white' />
              </SearchIcon>
              <form onSubmit={handleSubmit}>
                <input placeholder="Pesquisar" type="text" ref={inputRef}/>
              </form>
              <IoCloseSharp id="iconClose" size={25} color='white' onClick={() => { 
                setSearchBar(false); 
                uiContext?.setContent('initial')}}
              />
            </SearchBar>
          </>
        
        : <>
            <div/>
            <Logo>
              <img src={logotipo} alt="ioasys"/>
            </Logo>
            <SearchIcon>
              <IoSearchSharp size={40} color='white' onClick={() => {
                setSearchBar(true); 
                uiContext?.setContent('search')
                dataContext?.setData([]);
              }}/>
            </SearchIcon>
          </>
      }
    </Container>
  )
}

export default Header;