import styled, { css } from 'styled-components';


interface ContainerProps {
  searchBarOpen: boolean;
}

export const Container = styled.div<ContainerProps>`
  display: grid;
  grid-template-columns: 15% 70% 15%;

  ${props => props.searchBarOpen && css`
    display: flex;
    align-items: center;
    justify-content: center;
    grid-template-columns: 1fr;
  `}

  min-height: 9.5rem;
  width: 100%;
  background-color: #ee4c77;
`;

export const Logo = styled.div`
    display: flex;
    align-items: center;
    justify-content: center;
    
    img {
      height: 55px;
      width: 227px;
    }
`;

export const SearchIcon = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
`;

export const SearchBar = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  width: 90%;
  padding: 1px 8px;
  border-bottom: 0.6px solid var(--white-two);

  form ,input {
    height: 2.5rem;
    width: 90%;
    font-size: 2.125rem;
    background-color: #ee4c77;
    border: none;
    
    :focus {
      color: var(--white-two);
    }

    ::placeholder {
      color: #991237;
    }
  }

  #iconClose {
    cursor: pointer;
  }

`;