import styled from 'styled-components';

export const Container = styled.div`
  display: flex;
  align-items: center;
  min-width: 100%;

  margin: 1.2rem 0;
  padding: 4px;
  height: 2rem;

  color: #383743;
  font-size: 1.125rem;
  
  border-bottom: 0.6px solid #383743;

  input {
    min-width: 85%;
    opacity: 0.6;
    margin: 0 8px;
    background-color: #eeecdb;
    border: none;
    font-size: 1.2rem;
    :focus {
      opacity: 1;
    }
  }

  svg {
    color: #ee4c77;
  }
`;
