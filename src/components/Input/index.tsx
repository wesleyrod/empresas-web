import React, { InputHTMLAttributes } from 'react';
import { Container } from './styles';
import { useField } from '@unform/core';

import { IconBaseProps } from 'react-icons';
import { BsFillExclamationCircleFill } from 'react-icons/bs';
import { IoEyeOutline, IoEyeSharp } from 'react-icons/io5';



interface InputProps extends InputHTMLAttributes<HTMLInputElement> {
  name: string;
  icon?: React.ComponentType<IconBaseProps>;
  alert?: boolean;
}

const Input: React.FC<InputProps> = ({icon: Icon, alert, name, ...rest}) => {
  const [visible, setVisible] = React.useState(false);
  const inputRef = React.useRef<HTMLInputElement>(null);
  const {fieldName, defaultValue, error, registerField} = useField(name);

  React.useEffect(() => {
    registerField({
      name: fieldName,
      ref: inputRef.current,
      path: 'value',
    });
  }, [fieldName, registerField]);

  const toggleVisible = React.useCallback(() => {
    setVisible(!visible);
  },[visible]);

  return (
    <>
      <Container>
        {Icon && <Icon size={20}/>}
        
        {name.match('password')
          ? <input {...rest} type={ visible ? "text" : "password"} ref={inputRef} />
          : <input {...rest} ref={inputRef} />
        }

        {name.match('password') && visible && !alert
          ? <IoEyeSharp size={25} color="gray" onClick={toggleVisible}/>
          : <div/>
        }

        {name.match('password') && !visible && !alert
          ? <IoEyeOutline size={25} color="gray" onClick={toggleVisible}/>
          : <div/>
        }
        
        {alert && <BsFillExclamationCircleFill size={25} color="red"/>}
      </Container>
    </>
  )
};

export default Input;
