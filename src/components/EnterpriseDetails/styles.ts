import styled from 'styled-components';

export const Container = styled.div`
  display: flex;
  flex-direction: column;
  background-color: white;
  flex-grow: 1;
  width: 100%;
  padding: 3rem 4rem;

  img {
    max-width: 100%;
    max-height: 100%;
  }

  span {
    padding: 3rem 0;
    font-family: SourceSansPro;
    font-size: 2.125rem;
    font-weight: normal;
    font-stretch: normal;
    font-style: normal;
    line-height: normal;
    letter-spacing: normal;
    color: var(--warm-grey);
  }



`;
