import React from 'react';

import { Container } from './styles';

interface DetailsProps {
  photo: string;
  description: string;
}

const EntrepriseDetails: React.FC<DetailsProps> = (props) => {
  return (
    <Container>
      <img src={props.photo} alt="photo"/>
      <span>{props.description}</span>
    </Container>
  );
}

export default EntrepriseDetails;