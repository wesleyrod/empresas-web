import styled from 'styled-components';

export const Container = styled.div`
  display: flex;
  align-items: center;

  height: 200px;
  width: 100%;

  background-color: white;
  padding: 1.688rem 15.86rem 1.679rem 1.914rem;
  margin-bottom: 2rem;
  border-radius: 4.7px;
  line-height: 1.5rem;

  #image {
    img{
      width: 18.313rem;
      max-height: 10rem;
      margin: 0 2.397rem 0 0;
      padding: 0 0.04rem 0rem 0;
      object-fit: contain;
    }
  }

  #description {
    
    strong{
      font-size: 1.2rem;
      font-weight: normal;
      display: block;
      color: var(--warm-grey);
    }

    span {
      color: var(--warm-grey)
    }
  }

`;
