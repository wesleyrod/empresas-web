import React from 'react';
import { string } from 'yup';

import { Container } from './styles';

interface CardProps {
  photo: string;
  name: string;
  type: string;
  country: string;
}

const ResultCard: React.FC<CardProps> = (props) => {
  return (
    <Container>
      <div id="image">
        <img src={props.photo} alt="image"/>
      </div>
      <div id="description">
        <h2>{props.name}</h2>
        <strong>{props.type}</strong>
        <span>{props.country}</span>
      </div>      
    </Container>
  );
}

export default ResultCard;