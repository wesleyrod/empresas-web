import styled from 'styled-components';
import { darken } from 'polished';

export const Container = styled.button`
  height: 50px;
  border: none;
  border-radius: 3.6px;
  color: #fff;
  background-color: #57bbbc;
  font-weight: bold;
  margin: 2rem 1rem;

  :enabled{
    :hover {
     background: ${darken(0.1, '#57bbbc')};
    }
  }
  
  :disabled {
    background-color: #999;
    color: #777;
  }
`;
