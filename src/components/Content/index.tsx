import React from 'react';
import { UIContext } from '../../context/UIContext';
import { DataContext } from '../../context/DataContext';
import { Container, Home, SearchFailure, ListResults, EnterpriseView } from './styles';
import ResultCard from '../ResultCard';
import EnterpriseDetails from '../EnterpriseDetails';


interface EnterpriseTypeProps {
  enterprise_type_name: string;
}

interface EnterpriseProps {
  enterprise_name: string;
  country: string;
  photo: string;
  description: string;
  enterprise_type: EnterpriseTypeProps;
}

const Content: React.FC = () => {
  const uiContext = React.useContext(UIContext);
  const dataContext = React.useContext(DataContext);

  const listEnterprises:[] = dataContext?.data|| [];
  
  return (
    <Container>
    {uiContext?.content === 'initial' ? 
      <Home>
        <span>Clique na busca para iniciar.</span>
      </Home>
      :<div/>
    } 

    {uiContext?.content === 'search'  && listEnterprises.length > 0 ?
      <ListResults>
          {listEnterprises.map((e: EnterpriseProps) => {
            return (
              <ResultCard key={e.enterprise_name} photo={`https://empresas.ioasys.com.br${e.photo}`} name={e.enterprise_name} type={e.enterprise_type.enterprise_type_name} country={e.country}/>
            )
          })}
      </ListResults> 
      :<div/>
    }

    {uiContext?.content === 'erro' ?
      <SearchFailure>
        <span>Nenhuma empresa foi encontrada para a busca realizada.</span>
      </SearchFailure>
      :<div/>
    }

    {uiContext?.content === 'details' ?
      <EnterpriseView>
        <EnterpriseDetails photo="https://empresas.ioasys.com.br/uploads/enterprise/photo/1/240.jpeg" description="Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."/>
      </EnterpriseView>
      :<div/>
    }

    </Container>
  );
}

export default Content;