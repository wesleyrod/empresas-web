import styled from 'styled-components';

export const Container = styled.div`
  display: flex;
  justify-content: center;
  width: 100%;
  flex-grow: 1;
  background-color: #ebe9d7;
  padding: 3rem 3rem;
`;

export const Home = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  font-size: 2rem;
  color: var(--charcoal-grey);
`;

export const SearchFailure = styled.div`
  display: flex;
  margin-top: 200px;
  font-size: 2rem;
  color: var(--greyish);
`;

export const ListResults = styled.div`
  display: flex;
  align-items: center;
  flex-direction: column;
  flex-grow: 1;
  min-width: 100%;
`;

export const EnterpriseView = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  min-width: 100%;
`;

