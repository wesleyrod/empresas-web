import styled from 'styled-components';

export const Container = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  height: 100vh;
  width: 100vw;
  background-color: #eeecdb;
`;

export const LoginCard = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  width: 25rem;
  min-height: 65%;

  /* background-color: #ccc; */
`;

export const Logo = styled.div`
  margin-bottom: 4rem;
  img {
    width: 18.5rem;
    height: 4.5rem;
  }
`;

export const Welcome = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  max-width: 80%;
  text-align: center;
  line-height: 1.8rem;
  margin-bottom: 2rem;

  h2 {
    max-width: 70%;
    display: block;
    margin-bottom: 1.5rem;
  }

  span {
    font-size: 1.125rem;
  }
`;

export const FormContainer = styled.div`
  width: 100%;

  form {
    display: flex;
    flex-direction: column;
  }
`;
