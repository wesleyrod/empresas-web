import React from 'react';
import { useAuth } from '../../context/AuthContext';
import { Form } from '@unform/web';
import { Container, Logo, Welcome, LoginCard, FormContainer } from './styles';
import logo from '../../assets/logo-home@2x.png';
import { FiMail, FiLock } from 'react-icons/fi'

import Input from '../../components/Input';
import Button from '../../components/Button';


interface SignInData {
  email: string;
  password: string;
}

const Login: React.FC = () => {
  const { signIn } = useAuth();  

  const handleSubmit = React.useCallback(
    async (data: SignInData) => {
      try{
        signIn({
          email: data.email,
          password: data.password,
        })
      } catch(err) {
        console.log('Erro na autenticação');
      }
    },
    [signIn],
  )

  return (
    <>
      <Container>
        <LoginCard>
          <Logo>
            <img src={logo} alt="ioasys" />
          </Logo>

          <Welcome>
            <h2>BEM-VINDO AO EMPRESAS</h2>
            <span>
              Lorem ipsum dolor sit amet, contetur adipisicing elit. Nunc
              accumsan.
            </span>
          </Welcome>

          <FormContainer>
            <Form onSubmit={handleSubmit}>
              <Input name="email" type="email" placeholder="E-mail" icon={FiMail} alert/>
              <Input name="password" type="password" placeholder="Senha" icon={FiLock} />
              <Button type="submit">ENTRAR</Button>
            </Form>
          </FormContainer>

        </LoginCard>
      </Container>
    </>
  )
};

export default Login;
