import React from 'react';

import { Container } from './styles';
import Header from '../../components/Header';
import Content from '../../components/Content';

const Home: React.FC = () => {
  return (
    <Container>
      <Header/>
      <Content/>
    </Container>
  );

}

export default Home;