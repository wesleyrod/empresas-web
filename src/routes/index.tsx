import React from 'react';
import { Switch } from 'react-router-dom';
import Route from './Route';

import Login from '../Pages/Login';
import Home from '../Pages/Home';


const Routes: React.FC = () => (
  <Switch>
    <Route path='/' exact component={Login}/>
    <Route path='/home' component={Home} isPrivate/>
  </Switch>
)


export default Routes;