import React, { Component } from 'react';
import { useAuth } from '../context/AuthContext';
import { 
  RouteProps as ReactDOMRouterProps, 
  Route as ReactDOMRoute,
  Redirect
} from 'react-router-dom';

interface RouteProps extends ReactDOMRouterProps {
  isPrivate?: boolean;
  component: React.ComponentType;
}


const Route: React.FC<RouteProps> = ({ isPrivate = false, component: Component, ...rest}) => {
  const { token } = useAuth();

  return (
    <ReactDOMRoute 
      {...rest} 
      render={() => {
        return isPrivate === !!token ? (
          <Component />
        ) : (
          <Redirect to={{ pathname: isPrivate ? '/' : '/home'}} />
        );
      }}
    />
  );
};

export default Route;