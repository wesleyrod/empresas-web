import React from 'react';
import { BrowserRouter } from 'react-router-dom';
import Routes from './routes';
import GlobalStyle from './styles/global';
import { AuthProvider } from './context/AuthContext';
import { UIProvider } from './context/UIContext';
import { DataProvider } from './context/DataContext';

const App: React.FC = () => {
    
  return (
      <>
        <AuthProvider>
          <DataProvider>
            <UIProvider>
              <BrowserRouter>
                <Routes />
              </BrowserRouter>
            </UIProvider>
          </DataProvider>
        </AuthProvider>
        <GlobalStyle />
      </>
    )
}

export default App;
