import { createGlobalStyle } from 'styled-components';

export default createGlobalStyle`
  * {
    margin: 0;
    padding: 0;
    box-sizing: border-box;
    outline: 0;
  }

  :root {
    font-size: 16px;
    --white: #d8d8d8;
    --white-two: #ffffff;
    --dark-indigo: #1a0e49;
    --warm-grey: #8d8c8c;
    --charcoal-grey-two: #403e4d;
    --greyish: #b5b4b4;
    --charcoal-grey: #383743;
    --black-54: rgba(0, 0, 0, 0.54);
    --night-blue: #0d0430;
  }

  body {
    display: flex;
    align-items: center;
    justify-content: center;
    background: var(--charcoal-grey-two);
    color: #383743;
    -webkit-font-smothing: antialiased;
  }

  body, input, button {
    font-family: 'Roboto', sans-serif;
    font-size: 16px;
  }

  button {
    cursor: pointer;
  }

`;
